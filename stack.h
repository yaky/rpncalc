// stack.h by 4301 Akiyama
//デフォで定数を作る（今回は構造体を10000個）
#define N 10000
#include <math.h>
#include <stdio.h>


//構造体をつくる
typedef struct {
    double data[N];//detaという構造体をNコ作る倍精度浮動小数型だっけ？
    int top;//topという構造体を一つ作った。
} stack;

//プロトタイプ宣言
void initStack(stack *sp, int n);
double topValue(stack *sp);
void push(stack *sp, double val);
double pull(stack *sp);
void printStack(stack *sp);
void addStack(stack *sp);
void subStack(stack *sp);
void mulStack(stack *sp);
void divStack(stack *sp);