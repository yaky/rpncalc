// testStack.c by 4301Akiyama

#include <stdio.h>
#include <math.h>
#include "stack.h"
#include "testCommon.h"

void testInitStack() {
    stack myStack;
    testStart("initStack");
    myStack.top = 0; // 適当な値にセット
    initStack(&myStack, N);
    assert_equals_int(myStack.top, N); // sp が最終値より後ろになっているか
    testEnd();
}

void testTopValue(void) {
    stack myStack;
    testStart("topValue");
    initStack(&myStack, N);//myStackにNを入れる
    /* stack が空なら NaN を返す */
    assert_equals_int(isnan(topValue(&myStack)), 1);//isnanはNaNが返ったときに１を返す
    /* stack が空でなければスタックのさしている数字を返す */
    myStack.top = 0;
    myStack.data[0] = 5.0;
    assert_equals_int(isnan(topValue(&myStack)), 0); // not NaN
    assert_equals_double(topValue(&myStack), 5.0);
    testEnd();
}

void testPush(void) {
    stack myStack;
    testStart("Push");
    int a = 5;
    initStack(&myStack, N);//myStackにNを入れる
    push(&myStack, a);
    assert_equals_int(topValue(&myStack), 5);
    assert_equals_int(myStack.top, N-1);
    a = 3;
    push(&myStack, a);
    assert_equals_int(topValue(&myStack), 3);
    assert_equals_int(myStack.top, N-2);
    testEnd();
}


void testPull(void) {
    stack myStack;
    testStart("Pull");
    initStack(&myStack, N);//myStackにNを入れる
    push(&myStack, 5.0);
    push(&myStack, 3.0);
    assert_equals_int(pull(&myStack), 3.0);
    assert_equals_int(myStack.top, N-1);
    assert_equals_int(pull(&myStack), 5.0);
    assert_equals_int(myStack.top, N);
    assert_equals_int(isnan(pull(&myStack)), 1);
    assert_equals_int(myStack.top, N);    
    testEnd();
}

void testPrintStack(void) {
    stack myStack;
    testStart("printStack");
    initStack(&myStack, N);//myStackにNを入れる
    push(&myStack, 5.0);
    push(&myStack, 3.0);
    printStack(&myStack);    
    testEnd();
}

void testAddStack(void){
    stack myStack;
    testStart("AddStack");
    initStack(&myStack, N);//myStackにNを入れる
    push(&myStack, 4.0);
    push(&myStack, 2.0);
    addStack(&myStack);
    assert_equals_double(topValue(&myStack),6.0);
    assert_equals_int(myStack.top,N-1);
    addStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top,N-1);
    testEnd();
}

void testSubStack(void){
    stack myStack;
    testStart("SubStack");
    initStack(&myStack, N);//myStackにNを入れる
    push(&myStack, 4.0);
    push(&myStack, 2.0);
    subStack(&myStack);
    assert_equals_double(topValue(&myStack),2.0);
    assert_equals_int(myStack.top,N-1);
    subStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top,N-1);
    testEnd();
}

void testMulStack(void){
    stack myStack;
    testStart("MulStack");
    initStack(&myStack, N);//myStackにNを入れる
    push(&myStack, 4.0);
    push(&myStack, 2.0);
    mulStack(&myStack);
    assert_equals_double(topValue(&myStack),8.0);
    assert_equals_int(myStack.top,N-1);
    mulStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top,N-1);
    testEnd();
}

void testDivStack(void){
    stack myStack;
    testStart("DivStack");
    initStack(&myStack, N);//myStackにNを入れる
    push(&myStack, 4.0);
    push(&myStack, 2.0);
    divStack(&myStack);
    assert_equals_double(topValue(&myStack),2.0);
    assert_equals_int(myStack.top,N-1);
    divStack(&myStack);
    assert_equals_int(isnan(topValue(&myStack)), 1);
    assert_equals_int(myStack.top,N-1);
    testEnd();
}


int main() {
    testInitStack();
    testTopValue();
    testPush();
    testPull();
    testPrintStack();
    testAddStack();
    testSubStack();
    testMulStack();
    testDivStack();
	return 0;
}
