// stack.c by 4301 Akiyma
#include "stack.h"


//関数
void initStack(stack *sp, int n) {
    sp->top = n;//構造体のtopにnを入れる
}

double topValue(stack *sp) {
    if (sp->top == N) {//構造体のtopの中身がNであるあれば
        return sqrt(-1);//虚数を返すNaN
    }else{
        return sp->data[sp->top];//dataのtop番目の値をそのまま返す
    }
}
/*1.topが1つ減ること
  2.topの場所に渡されること
 */
void push(stack *sp, double val) {
    sp->top = sp->top - 1;
    sp->data[sp->top] = val;//dataのtop番目の値をそのまま返す
}

double pull(stack *sp) {
    double n;
    
    if (sp->top==N) {
        n = sqrt(-1);
        return n;
    }else{
        n = sp->data[sp->top];
        sp->top = sp->top + 1;
        return n;
    }
    
    
}

void printStack(stack *sp){
    int i;
    for (i = N-1; i >= sp->top; i--) {
        printf("%g\n",sp->data[i]);
    }
}

void addStack(stack *sp){
    push(sp,pull(sp)+pull(sp));
}

void subStack(stack *sp){
    double x, y;
    x = pull(sp);
    y = pull(sp);
    push(sp,y-x);
}

void mulStack(stack *sp){
    push(sp,pull(sp)*pull(sp));
}

void divStack(stack *sp){
    double x, y;
    x = pull(sp);
    y = pull(sp);
    push(sp,y/x);
}





